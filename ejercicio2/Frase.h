#include <iostream>
using namespace std;

#ifndef FRASE_H
#define FRASE_H

class Frase {
    private:
        string oracion = "";

    public:
        Frase ();
        Frase (string oracion);
        string get_oracion();
        void set_oracion(string oracion);
};
#endif
