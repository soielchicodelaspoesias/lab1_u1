#include <iostream>
#include <cstdlib>
#include <string>
#include "Frase.h"

// funcion que cuenta las mayusculas y minusculas
void contar_mayuscualas_minusculas(string line){
  int upper = 0;
  int lower = 0;
  for (int j=0; j<line.size();j++){
    // si es mayuscula la cuenta
    if(line[j] >= 'A' && line[j] <= 'Z'){
      upper = upper + 1;
    }
    // cuenta las minusculas
    if(line[j] >= 'a' && line[j] <= 'z'){
      lower = lower + 1;
    }
  }
  cout << "Mayusculas: " << upper << endl;
  cout << "Minusculas: " << lower << endl;

}
// se imprimen las oraciones y llama a la funcion que cuenta mayusculas y minusculas
void imprimir_arreglo(Frase *oraciones, int n){
    int contador = 1;
    for (int i = 0; i < n; i++) {
      cout << "----ORACION " << contador<< "-----" << endl;
      cout << oraciones[i].get_oracion() << endl;
      string line = oraciones[i].get_oracion();
      contar_mayuscualas_minusculas(line);
      contador = contador + 1;
  }
}
// se ingresan n cantidad de frases
void insertar_oraciones(Frase *oraciones, int n){
  Frase f = Frase();
  char line[15];
  int contador = 1;
  for(int i=0; i<n; i++){
    cout << "Oracion " << contador << ": ";
    if(i==0){
      // el getline se come una linea asi que eso lo soluciona
      cin.ignore();
    }
    cin.getline(line, 15);
    f.set_oracion(line);
    oraciones[i] = f;
    contador = contador + 1;

  }
}

int main () {
    int opcion;
    int n;

    cout << "Ingrese la cantidad de oraciones: ";
    cin >> n;
    Frase oraciones[n];

    insertar_oraciones(oraciones, n);
    system("clear");
    imprimir_arreglo(oraciones, n);
}
