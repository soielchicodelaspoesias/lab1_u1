#include <iostream>
using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente {
    private:
        string nombre = "";
        int telefono = 0;
        int saldo = 0;
        bool moroso;

    public:
        Cliente ();
        Cliente (string nombre, int telefono, int saldo, bool moroso);
        string get_nombre();
        int get_telefono();
        int get_saldo();
        bool get_moroso();
        void set_nombre(string nombre);
        void set_telefono(int telefono);
        void set_saldo(int saldo);
        void set_moroso(bool moroso);
};
#endif
