#include <iostream>
#include <cstdlib>
#include "Clientes.h"


// imprime los clientes
void imprimir_clientes(Cliente *clientes, int n){
    for (int i = 0; i < n; i++) {
      if(i != 0){
      cout << "-----------------" << '\n';
      }
      cout << "Nombre: " << clientes[i].get_nombre() << endl;
      cout << "Telefono: "<< clientes[i].get_telefono() << endl;
      cout << "Saldo: "<< clientes[i].get_saldo() << endl;
      if(clientes[i].get_moroso() == 1){
        cout << "moroso" << endl;
      }
      else{
        cout << "No moroso" << endl;
      }
    }
}

// Se agregan los clientes
void agregar_clientes (Cliente *clientes, int n) {
  string line;
  int line2;
  int line3;
  bool line4;
  Cliente c = Cliente();

  for(int i=0;i<n;i++) {

  cout << "Nombre: ";
  cin >> line;
  cout << "Telefono: ";
  cin >> line2;
  cout << "Saldo: ";
  cin >> line3;
  cout << "Moroso (1 si es moroso, 0 si no es moroso): ";
  cin >> line4;
  c.set_nombre(line);
  c.set_telefono(line2);
  c.set_saldo(line3);
  c.set_moroso(line4);
  // se agrega cada cliente al arreglo
  clientes[i] = c;

  }
}

int main () {
    int opcion;
    int n;

    cout << "Ingrese la cantidad de clientes: ";
    cin >> n;
    Cliente clientes[n];

    agregar_clientes(clientes, n);
    system("clear");
    cout << "----CLIENTES-----" << endl;
    imprimir_clientes(clientes, n);
}
