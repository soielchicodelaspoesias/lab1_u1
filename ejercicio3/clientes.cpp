#include <iostream>
#include "Clientes.h"
using namespace std;

Cliente::Cliente() {
    string nombre = "";
    int telefono = 0;
    int saldo = 0;
    bool moroso = 0;
}

Cliente::Cliente (string nombre, int telefono, int saldo, bool moroso ) {
    this->nombre = nombre;
    this->telefono = telefono;
    this->saldo = saldo;
    this->moroso = moroso;

}

string Cliente::get_nombre() {
    return this->nombre;
}

int Cliente::get_telefono() {
    return this->telefono;
}

int Cliente::get_saldo() {
    return this->saldo;
}

bool Cliente::get_moroso() {
    return this -> moroso;
}

void Cliente::set_nombre(string nombre) {
    this->nombre = nombre;
}

void Cliente::set_telefono(int telefono) {
    this->telefono = telefono;
}

void Cliente::set_saldo(int saldo) {
    this->saldo = saldo;
}
void Cliente::set_moroso(bool moroso) {
    this->moroso = moroso;
}
