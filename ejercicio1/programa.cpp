#include <iostream>
#include <cstdlib>
#include "Lista.h"


// Funcion que imprime el arreglo
void imprimir_arreglo(Arreglo *cuadrados, int n){
    for (int i = 0; i < n; i++) {
      cout << cuadrados[i].get_numero() << " ";
    }
}

// Se llena el arreglo con el cuadrado de los numeros
void inicializa_arreglo(Arreglo *cuadrados, int n){
  Arreglo a = Arreglo();

  for(int i=0; i<n; i++){
    a.set_numero((i+1)*(i+1));
    cuadrados[i] = a;
  }
}

int main () {
    int opcion;
    int n;
    // Se ingresa el tamaño de la lista
    cout << "Ingrese la cantidad de numero: ";
    cin >> n;
    Arreglo cuadrados[n];

    inicializa_arreglo(cuadrados, n);
    system("clear");
    cout << "----CUADRADOS-----" << endl;
    imprimir_arreglo(cuadrados, n);
}
