#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Arreglo {
    private:
        int numero = 0;

    public:
        Arreglo ();
        Arreglo (int numero);
        int get_numero();
        void set_numero(int numero);
};
#endif
