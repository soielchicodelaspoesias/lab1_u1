# laboratorio1

*Calcula cuadrados*                                                                                             
(C.C)

Calcula el cuadrado de n numeros                                                                        

*Pre-requisitos* 

C++
make                                                                                                                                                                                                                                           

*Ejecutando*                                                                                                                                         
Al ejecutarlo el programa le pedira un numero, y calculara el cuadrado desde el 1 hasta el numero que ingreso, los mostrara en la pantalla

*Construido con*                                                                                                                                    
C++                                                                                                           

*Versionado*                                                                                                                                        
Version 0,1                                                                                                                                       

*Autor*                                                                                                                                           
Luis Rebolledo                                                                                                                                                                                                                                                                                                                        

